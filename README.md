# proyectoPrueba

## Descripción

** Aplicación Web de prueba  con Spring Framework 4**

	-Consulta y creación de usuarios
	-Responde servicios Rest
	-Envío y recepción de información en formato Json
	-Requiere autenticación básica
	-Desplegable en contenedor Docker

## Versiones de software utilizadas

	- Java 8
	-Maven 3.6.0
	-Docker 19.03.6

## Instrucciones
Ver código fuente:
https://gitlab.com/aeggit/simpleuserapp/-/tree/master/


Obtener el código fuente
https://gitlab.com/aeggit/simpleuserapp.git

Compilar y generar ejecutable:
```bash
mvn clean package tomcat7:exec-war-only
```
Para probar la aplicación de forma local
```bash
java -jar target/SimpleUserMaven.jar
```
En este punto se accede desde localhost. 
http://localhost:8080/simpleapp/users  

-ver la sección de **Verificación** para conocer los casos de prueba

Para construir la imagen con DockerFile:
```bash
docker build -t usuario_docker/simpleuserapp:dockerfile .
```

Para ejecutar la imagen Docker:
```bash
docker run -p 8080:8080 usuario_docker/simpleuserapp:dockerfile
```

## Verificación


Lista de usuarios disponibles para consulta:
```
user1
user2
Claudia
Iris
```
Para todos los usuarios el password es **“password”**

**Para conocer la IP del docker_host:**
```bash
docker ps
docker inspect container_id
```

**Para verificar en el navegador de docker host:** 

```
GET http://docker_host:8080/simpleapp/users
GET http://docker_host:8080/simpleapp/user/{id}
GET http://docker_host:8080/simpleapp/usersByAuth

```

```
POST http://docker_host:8080/simpleapp/user
	body in Json
{
"name": "Brenda",
"country": "Colombia",
"creationDate": 1605518981416
}
```

Probar con el usuario user1
```
GET http://docker_host:8080/simpleapp/usersByAuth
```

Probar con el usuario Claudia
```
GET http://docker_host:8080/simpleapp/usersByAuth
```

Probar con el usuario Iris
```
GET http://docker_host:8080/simpleapp/usersByAuth
```
