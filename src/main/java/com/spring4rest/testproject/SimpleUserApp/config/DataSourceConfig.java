/**
 * 
 */
package com.spring4rest.testproject.SimpleUserApp.config;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.h2.tools.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.stereotype.Component;

/**
 * @author AEG
 *
 */
@Component
public class DataSourceConfig {

	@Bean
	public DataSource dataSource() {
		return new EmbeddedDatabaseBuilder()
				.generateUniqueName(true)
				.setType(EmbeddedDatabaseType.H2)
				.setScriptEncoding("UTF-8")
				.ignoreFailedDrops(true)
				.addScript("db/h2/create-db.sql")
				.addScripts("db/h2/insert-data.sql")
				.build();
	}
	
/*
	// Start WebServer, access http://localhost:8082
	@Bean(initMethod = "start", destroyMethod = "stop")
	public Server startDBManager() throws SQLException {
		Server server = Server.createWebServer(); // arg "-webPort", "8083"
		if(!server.isRunning(true)) {
			System.out.println("El servidor H2 esta detenido");
			server.start();
			System.out.println("El servidor H2 iniciado");
		}
		//return Server.createWebServer("-webPort", "8083");
		return server;
	}
	*/
	
}
