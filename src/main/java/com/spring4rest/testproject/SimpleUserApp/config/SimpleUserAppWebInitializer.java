package com.spring4rest.testproject.SimpleUserApp.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;


/**
 * @author AEG
 *
 */

public class SimpleUserAppWebInitializer extends AbstractAnnotationConfigDispatcherServletInitializer{

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class<?>[] {RootConfig.class};
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[] {WebConfig.class};
	}

	/**
	 * Define the path to Map the DispatcherServlet
	 */
	@Override
	protected String[] getServletMappings() {
		return new String[] {"/"};
	}

}
