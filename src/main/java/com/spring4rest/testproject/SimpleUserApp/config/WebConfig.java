package com.spring4rest.testproject.SimpleUserApp.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author AEG
 *
 */


@Configuration
@EnableWebMvc
@ComponentScan("com.spring4rest.testproject.SimpleUserApp")
public class WebConfig extends WebMvcConfigurerAdapter{

	/**
	 * Next configuration is to avoid manage the statics resources by the DispatcherServlet
	 */
	@Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }	
	
	@Override
    public void addFormatters(FormatterRegistry registry) {
		//String timePattern = "hh:mm:ss a";
		//DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(timePattern);
        registry.addFormatter(new DateFormatter("yyyyMMdd"));
    }
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}	
	
	
}
