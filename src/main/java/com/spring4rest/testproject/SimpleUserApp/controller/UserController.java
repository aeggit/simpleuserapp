package com.spring4rest.testproject.SimpleUserApp.controller;

import java.net.URI;
import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.spring4rest.testproject.SimpleUserApp.entity.User;
import com.spring4rest.testproject.SimpleUserApp.exception.UserNotFoundException;
import com.spring4rest.testproject.SimpleUserApp.service.IUserService;
import com.spring4rest.testproject.SimpleUserApp.service.UserService;

/**
 * 
 * @author AEG
 *
 */
@RestController
@RequestMapping("/simpleapp")
public class UserController {

	@Autowired
	private UserService userService;
	
	@Autowired
	@Qualifier(value = "userServiceImpl")
	private IUserService service;
	
	@GetMapping("/defaultUser")
	public String getdefaultUser() {
		return "Hello default User with executable war dockerized prueba 2";
	}
	
	// Get list with all users - GET /users
	@GetMapping("/users")
	public List<User> findAll(){
		//return service.findAll();
		return userService.findAll();
	}
	
	// Get list users same country as requested
	@GetMapping("/usersByAuth")
	public List<User> findAllByCountry(Principal principal, Authentication authentication, HttpServletRequest request){
		String name = principal.getName();
		System.out.println("Usuario obtenido por el Principal: " + name);
		
//		String name2 =  authentication.getName();
//		System.out.println("Usuario obtenido por el autentication: " + name2);
		
//		Principal principal2 = request.getUserPrincipal();
//		String name3 = principal2.getName();
//		System.out.println("Usuario obtenido por el request: " + name3);
		
		return userService.findByCountry(name);
	}
	
	// Get user by id - GET /users/{userId}
	@GetMapping("/user/{userId}")
	public User getUserById(@PathVariable Long userId) {
		//User user = service.findById(userId);
		User user = userService.findById(userId);
		System.out.println("User en GetUserById en el controlador: " + user);
		if(user == null) {
			throw new UserNotFoundException("id - " + userId);
		}
		return user;
	}
	
	// Create new User - POST /user
	@PostMapping("/user")
	public ResponseEntity<Object> saveUser(@Valid @RequestBody User user) {
		System.out.println("New user: " + user);
		User newUser = userService.save(user);
		System.out.println("Usuario salvado: " + newUser);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(newUser.getId()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	// Delete user by Id - DELETE /user/{id}
	@DeleteMapping("/user/{id}")
	public ResponseEntity<String> deleteUserById(@PathVariable int id) {
		User user = userService.deleteUserById(id);
		System.out.println("Usera borrar en deleteUserById en el controlador: " + user);
		if(user == null) {
			throw new UserNotFoundException("id - " + id);
		}
		return new ResponseEntity<String>("Usuario eliminado", HttpStatus.NO_CONTENT); 
	}

	
	
}
