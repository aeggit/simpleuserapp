package com.spring4rest.testproject.SimpleUserApp.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spring4rest.testproject.SimpleUserApp.entity.User;

/**
 * @author AEG
 *
 */
@Repository
@Transactional
public interface UserDao extends CrudRepository<User, Long>{

}
