/**
 * 
 */
package com.spring4rest.testproject.SimpleUserApp.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;



/**
 * @author AEG
 *
 */
@Entity
@Table(name = "usuarios")
public class User implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Size(min = 3, message = "Al menos debe tener 3 caracteres")
	@NotEmpty
	@Column(name = "nombre")
	private String name;
	
	@Size(min = 2, message = "Al menos debe tener 2 caracteres")
	@NotEmpty
	@Column(name = "pais")
	private String country;
	
	@Column(name = "autorizado")
	private boolean autorized;
	
	@DateTimeFormat
	@Past
	@Column(name = "fecha_creacion")
	@Temporal(TemporalType.DATE)
	private Date creationDate;

	/**
	 * Default Constructor
	 */
	public User() {
	}
	
	/**
	 * Constructor using fields 
	 * @param name
	 * @param country
	 * @param creationDate
	 */
	public User(Long id, String name, String country, Date creationDate) {
		this.id = id;
		this.name = name;
		this.country = country;
		this.creationDate = creationDate;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	/**
	 * @return the autorized
	 */
	public boolean isAutorized() {
		return autorized;
	}

	/**
	 * @param autorized the autorized to set
	 */
	public void setAutorized(boolean autorized) {
		this.autorized = autorized;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", country=" + country + ", creationDate=" + creationDate + "]";
	}
	
	private static final long serialVersionUID = -4311756091909881092L;
	
	
}
