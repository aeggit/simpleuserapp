/**
 * 
 */
package com.spring4rest.testproject.SimpleUserApp.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * @author AEG
 *
 */
public class MessageSecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {

}
