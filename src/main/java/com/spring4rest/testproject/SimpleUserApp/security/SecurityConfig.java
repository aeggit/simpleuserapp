/**
 * 
 */
package com.spring4rest.testproject.SimpleUserApp.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * @author osboxes
 *
 */

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{

	
	@Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        
		auth.inMemoryAuthentication().withUser("user1").password("password").roles("USER");
		auth.inMemoryAuthentication().withUser("user2").password("password").roles("ADMIN");
		auth.inMemoryAuthentication().withUser("Claudia").password("password").roles("USER");
		auth.inMemoryAuthentication().withUser("Iris").password("password").roles("ADMIN");
		
    }
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
        //http.csrf().disable();
	    http
	       .httpBasic().and()
	       .authorizeRequests()
	       .antMatchers(HttpMethod.POST, "/simpleapp" , "/simpleapp/**")
	       .permitAll().anyRequest().authenticated()
	       .and().csrf().disable();

        
        /*
http
  .httpBasic().and()
  .authorizeRequests()
    .antMatchers(HttpMethod.POST, "/simpleapp").hasRole("ADMIN")
    .antMatchers(HttpMethod.PUT, "/employees/**").hasRole("ADMIN")
    .antMatchers(HttpMethod.PATCH, "/employees/**").hasRole("ADMIN");
         */
        
    }
	
}
