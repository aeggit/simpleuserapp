/**
 * 
 */
package com.spring4rest.testproject.SimpleUserApp.service;

import java.util.List;

import com.spring4rest.testproject.SimpleUserApp.entity.User;

/**
 * @author AEG
 *
 */
public interface IUserService {

	public List<User> findAll();
	public User findById(Long id);
}
