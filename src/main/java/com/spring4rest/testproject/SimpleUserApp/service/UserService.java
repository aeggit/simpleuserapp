/**
 * 
 */
package com.spring4rest.testproject.SimpleUserApp.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;

import com.spring4rest.testproject.SimpleUserApp.entity.User;

/**
 * 
 * @author AEG
 *
 */
@Component
public class UserService {
	
	private static List<User> userList = new ArrayList<User>();
	
	static {
		userList.add(new User(1l, "Claudia", "México", new Date()));
		userList.add(new User(2l, "Fernando", "México", new Date()));
		userList.add(new User(3l, "Iris", "Colombia", new Date()));
	}
	
	private static Long userCount = 3L;
	
	/**
	 * Looks for all users
	 * @return
	 */
	public List<User> findAll(){
		return userList;
	}
	
	/**
	 * Return List<User> with all user related to country
	 * @param country
	 * @return
	 */
	public List<User> findByCountry(String name){
		List<User> newList = new ArrayList<User>();
		User userName = this.findUserByName(name);
		if(userName != null) {
			for(User user : userList) {
				if(user.getCountry().equals(userName.getCountry())) {
					newList.add(user);
				}
			}
		}
		return newList;
	}
	
	/**
	 * Return User By Name
	 * @param name
	 * @return
	 */
	public User findUserByName(String name) {
		User userName = null;
		for(User user : userList) {
			if(user.getName().equals(name)) {
				userName = user;
				break;
			}
		}
		return userName;
	}
	
	/**
	 * find a user by id
	 * @param id
	 * @return
	 */
	public User findById(Long id) {
		for(User user : userList) {
			if(user.getId() == id) {
				return user;
			}
		}
		return null;
	}
	
	/**
	 * Save a user
	 * @param user
	 * @return
	 */
	public User save(User user) {
		//System.out.println("Save in Service- valor del Id: " + Long.valueOf(user.getId()));
		if(user.getId() == null) {
			System.out.println("El Id del usuario es nulo");
		}
		if(user.getId() == null || user.getId() <= 0 ) {
			user.setId(++userCount);
			System.out.println("Agregando usuario");
			userList.add(user);
		}
		System.out.println("Regresando el usuario guardado");
		return user;
	}

	/**
	 * Delete user by Id
	 * @param id
	 * @return
	 */
	public User deleteUserById(int id) {
		Iterator<User> iterator = userList.iterator();
		while(iterator.hasNext()) {
			User user = iterator.next();
			if(user.getId() == id) {
				userList.remove(user);
				return user;
			}
		}
		return null;
	}

	
}
