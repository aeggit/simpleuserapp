/**
 * 
 */
package com.spring4rest.testproject.SimpleUserApp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring4rest.testproject.SimpleUserApp.dao.UserDao;
import com.spring4rest.testproject.SimpleUserApp.entity.User;

/**
 * @author AEG
 *
 */
@Service
public class UserServiceImpl implements IUserService{

	//@Autowired
	private UserDao userDao; 
	
	@Override
	@Transactional(readOnly = true)
	public List<User> findAll() {
		return (List<User>) userDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public User findById(Long id) {
		return userDao.findOne(id);
	}

}
