--DROP TABLE users IF EXISTS;

CREATE TABLE usuarios (
  id  INTEGER PRIMARY KEY,
  nombre VARCHAR(30),
  pais  VARCHAR(20),
  autorizado BOOLEAN,
  fecha_creacion DATE
);